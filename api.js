var head = new Headers();
head.append("Access-Control-Allow-Origin", "*");
head.append("Accept", "application/json");
head.append("Authorization", "Bearer aad3c7af30b268a5539acf18fb94809d152b9225");
const proxyurl = "https://cors-anywhere.herokuapp.com/";

fetch(proxyurl + "https://devshop-376948.shoparena.pl/webapi/rest/products?limit=16", {
    method: "GET",
    headers: head,
    redirect: "follow",
})
    .then((response) => response.text())
    .then((result) => {
        var products = JSON.parse(result);
        var i;
        for (i in products.list) {
            var prod =
                "<p class='product__list-item__subtitle'>Product_id:</p>" +
                products.list[i].translations.pl_PL.product_id +
                "<br><p class='product__list-item__subtitle'>Nazwa:</p>" +
                products.list[i].translations.pl_PL.name +
                "<br><p class='product__list-item__subtitle'>Opis:</p>" +
                products.list[i].translations.pl_PL.short_description +
                "<br><p class='product__list-item__subtitle'>Cena:</p>" +
                products.list[i].stock.price +
                "<br><p class='product__list-item__subtitle'>Data dodania:</p>" +
                products.list[i].add_date +
                "<br><p class='product__list-item__subtitle'>Kategoria id:</p>" +
                products.list[i].category_id;
            function newfunction() {
                document.getElementsByClassName("product__list-item")[i].innerHTML = prod;
            }
            newfunction();
        }
    })
    .catch((error) => console.log("error", error));
